package com.example.stockmarketcompose.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.stockmarketcompose.util.COMPANY_TABLE_NAME

@Dao
interface StockDao {

    @Insert(onConflict = REPLACE)
    suspend fun insertCompanyListings(
        companyListingEntities: List<CompanyListingEntity>
    )

    @Query("DELETE  FROM $COMPANY_TABLE_NAME")
    suspend fun clearCompanyListing()

    @Query(
        """
         SELECT * FROM $COMPANY_TABLE_NAME 
         WHERE LOWER(name) LIKE '%' || LOWER(:query) || '%' OR
         UPPER(:query) == symbol            
    """
    )
    suspend fun searchCompanyListing(query: String): List<CompanyListingEntity>
}