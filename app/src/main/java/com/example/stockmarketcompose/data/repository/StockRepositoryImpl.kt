package com.example.stockmarketcompose.data.repository

import com.example.stockmarketcompose.data.csv.CSVParser
import com.example.stockmarketcompose.data.local.StockDatabase
import com.example.stockmarketcompose.data.mapper.toCompanyInfo
import com.example.stockmarketcompose.data.mapper.toCompanyListing
import com.example.stockmarketcompose.data.mapper.toCompanyListingEntity
import com.example.stockmarketcompose.data.remote.StockApi
import com.example.stockmarketcompose.domain.model.CompanyInfo
import com.example.stockmarketcompose.domain.model.CompanyListing
import com.example.stockmarketcompose.domain.model.IntradayInfo
import com.example.stockmarketcompose.domain.repository.StockRepository
import com.example.stockmarketcompose.util.Resource
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okio.IOException
import retrofit2.HttpException

@Singleton
class StockRepositoryImpl @Inject constructor(
    private val api: StockApi,
    private val db: StockDatabase,
    private val companyListingsParser: CSVParser<CompanyListing>,
    private val intradayInfoParser: CSVParser<IntradayInfo>,
) : StockRepository {
    private val dao = db.stockDao()

    override suspend fun getCompanyListings(
        fetchFromRemote: Boolean,
        query: String
    ): Flow<Resource<List<CompanyListing>>> {
        return flow {
            emit(Resource.Loading())

            val localListings = dao.searchCompanyListing(query = query)

            emit(Resource.Success(data = localListings.map { it.toCompanyListing() }))

            val isDbEmpty = localListings.isEmpty() && query.isBlank()

            val shouldLoadFromCache = !isDbEmpty && !fetchFromRemote

            if (shouldLoadFromCache) {
                emit(Resource.Loading(isLoading = false))
                return@flow
            }
            val remoteListings = try {
                val response = api.getListings()

                companyListingsParser.parse(response.byteStream())

            } catch (e: IOException) {
                e.printStackTrace()
                emit(Resource.Loading(isLoading = false))
                emit(Resource.Error(message = "Couldn't load data: ${e.message}"))
                null
            } catch (e: HttpException) {
                emit(Resource.Loading(isLoading = false))
                emit(Resource.Error(message = "Network error: ${e.message}"))
                null
            }

            remoteListings?.let { listings ->
                dao.clearCompanyListing()

                dao.insertCompanyListings(
                    listings.map { it.toCompanyListingEntity() }
                )

                emit(
                    Resource.Success(
                        data = dao
                            .searchCompanyListing("")
                            .map {
                                it.toCompanyListing()
                            })
                )

                emit(Resource.Loading(isLoading = false))
            }
        }
    }

    override suspend fun getIntradayInfo(symbol: String): Resource<List<IntradayInfo>> {
        return try {
            val response = api.getIntradayInfo(symbol = symbol)
            val results = intradayInfoParser.parse(response.byteStream())
            Resource.Success(data = results)

        } catch (e: IOException) {
            e.printStackTrace()
            Resource.Error(message = "Couldn't load data: ${e.message}")
        } catch (e: HttpException) {
            Resource.Error(message = "Network error: ${e.message}")
        }
    }

    override suspend fun getCompanyInfo(symbol: String): Resource<CompanyInfo> {
        return try {
            val result = api.getCompanyInfo(symbol = symbol)
            Resource.Success(data = result.toCompanyInfo())

        } catch (e: IOException) {
            e.printStackTrace()
            Resource.Error(message = "Couldn't load data: ${e.message}")
        } catch (e: HttpException) {
            Resource.Error(message = "Network error: ${e.message}")
        }
    }
}