package com.example.stockmarketcompose.data.remote

import com.example.stockmarketcompose.data.remote.dto.CompanyInfoDto
import com.example.stockmarketcompose.util.API_KEY
import com.example.stockmarketcompose.util.GET_COMPANY_INFO
import com.example.stockmarketcompose.util.GET_INTRADAY
import com.example.stockmarketcompose.util.GET_LISTING
import com.example.stockmarketcompose.util.QUERY_API_KEY
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface StockApi {

    @GET(GET_LISTING)
    suspend fun getListings(
        @Query(QUERY_API_KEY) apiKey: String = API_KEY
    ): ResponseBody

    @GET(GET_INTRADAY)
    suspend fun getIntradayInfo(
        @Query("symbol") symbol: String,
        @Query("apikey") apiKey: String = API_KEY
    ): ResponseBody

    @GET(GET_COMPANY_INFO)
    suspend fun getCompanyInfo(
        @Query("symbol") symbol: String,
        @Query("apikey") apiKey: String = API_KEY
    ): CompanyInfoDto
}