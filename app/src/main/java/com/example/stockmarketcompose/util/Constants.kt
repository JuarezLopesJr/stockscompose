package com.example.stockmarketcompose.util

// Retrofit
const val QUERY_API_KEY = "apikey"
const val API_KEY = "Q63Y9NX3TUF587NF"
const val BASE_URL = "https://alphavantage.co"
const val GET_LISTING = "query?function=LISTING_STATUS"
const val GET_INTRADAY = "query?function=TIME_SERIES_INTRADAY&interval=60min&datatype=csv"
const val GET_COMPANY_INFO = "query?function=OVERVIEW"

// Room
const val COMPANY_TABLE_NAME = "companylistingentity"
const val COMPANY_DATABASE = "company_database"