package com.example.stockmarketcompose.di

import com.example.stockmarketcompose.data.csv.CSVParser
import com.example.stockmarketcompose.data.local.StockDatabase
import com.example.stockmarketcompose.data.remote.StockApi
import com.example.stockmarketcompose.data.repository.StockRepositoryImpl
import com.example.stockmarketcompose.domain.model.CompanyListing
import com.example.stockmarketcompose.domain.model.IntradayInfo
import com.example.stockmarketcompose.domain.repository.StockRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    /* config for custom interface is done using @Binds, better performance from Hilt
    @Provides
     @Singleton
     fun provideCSVParser(): CSVParser<CompanyListing> {
         return CompanyListingsParser()
     }*/

    @Provides
    @Singleton
    fun provideRepository(
        api: StockApi,
        db: StockDatabase,
        companyListingsParser: CSVParser<CompanyListing>,
        intradayInfoParser: CSVParser<IntradayInfo>
    ): StockRepository {
        return StockRepositoryImpl(
            api,
            db,
            companyListingsParser,
            intradayInfoParser
        )
    }
}