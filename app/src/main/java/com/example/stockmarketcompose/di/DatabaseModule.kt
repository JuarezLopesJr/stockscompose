package com.example.stockmarketcompose.di

import android.content.Context
import androidx.room.Room
import com.example.stockmarketcompose.data.local.StockDatabase
import com.example.stockmarketcompose.util.COMPANY_DATABASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ): StockDatabase = Room.databaseBuilder(
        context,
        StockDatabase::class.java,
        COMPANY_DATABASE
    ).build()
}